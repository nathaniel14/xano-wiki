import {Component, EventEmitter, OnInit} from '@angular/core';
import {WikiService} from '../wiki.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {concatMap, finalize} from 'rxjs/operators';
import {faArrowLeft, faClock, faEdit, faExclamation, faSpinner} from '@fortawesome/free-solid-svg-icons';
import {ConfigService} from '../config.service';
import {LoginPanelComponent} from '../login-panel/login-panel.component';
import {PanelService} from '../shared/panel/panel.service';
import {ToastService} from '../shared/toast/toast.service';
import {WikiManagePanelComponent} from '../wiki-manage-panel/wiki-manage-panel.component';

@Component({
	selector: 'app-wiki-view',
	templateUrl: './wiki-view.component.html',
	styleUrls: ['./wiki-view.component.scss']
})
export class WikiViewComponent implements OnInit {
	public wiki: any;
	public latestWiki: any;
	public faEdit = faEdit;
	public faExclamation = faExclamation;
	public faSpinner = faSpinner;
	public faClock = faClock;
	public faArrowLeft = faArrowLeft;
	public loggedIn: boolean = false;
	public loading: boolean = true;
	public showHistory: boolean = false;
	public wikiHistory: any = [];

	constructor(
		private wikiService: WikiService,
		private route: ActivatedRoute,
		private router: Router,
		private configService: ConfigService,
		private toastService: ToastService,
		private panelService: PanelService
	) {
	}

	ngOnInit(): void {
		this.configService.isLoggedIn().subscribe(loggedIn => this.loggedIn = loggedIn);
		this.route.params.pipe(concatMap(params => this.wikiService.wikiGet(params.slug).pipe(finalize(() => this.loading = false)
		))).subscribe(wiki => {
			this.latestWiki = wiki;
			this.wiki = wiki;
		});
	}

	public login() {
		LoginPanelComponent.open(this.panelService, {});
	}

	public addWiki(): void {
		const onSuccess = new EventEmitter();
		onSuccess.subscribe(payload => {
			this.toastService.success('Wiki added.');
			setTimeout(() => {
				this.router.navigate(['wiki', payload.slug]);
			}, 300);
		});

		WikiManagePanelComponent.open(this.panelService, {
			item: null,
			successEmitter: onSuccess
		});
	}

	public editWiki(item) {
		const onSuccess = new EventEmitter();
		onSuccess.subscribe(payload => {
			this.toastService.success('Wiki updated.');
			this.router.navigate(['wiki', payload.slug]);
			this.getWiki(payload.slug);
		});

		const onDelete = new EventEmitter();
		onDelete.subscribe(payload => {
			this.toastService.success('Wiki deleted.');
		});

		WikiManagePanelComponent.open(this.panelService, {
			item,
			successEmitter: onSuccess,
			deleteEmitter: onDelete
		});
	}

	public getWiki(slug) {
		 this.wikiService.wikiGet(slug).toPromise().then(wiki => {
			this.wiki = wiki;
		});
	}

	public showHistoryFn(wikiId) {
		this.showHistory = !this.showHistory;
		if (this.showHistory) {
			// console.log('hi')
			this.wikiService.wikiHistoryGet(wikiId).subscribe(res => this.wikiHistory = res);
		}
	}

	public viewVersion(wiki) {
		this.wiki = wiki;
		this.showHistory = false;
	}
}
