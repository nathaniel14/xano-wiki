import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

export interface XanoConfig {
	title: string;
	summary: string;
	editText: string;
	editLink: string;
	descriptionHtml: string;
	logoHtml: string;
	requiredApiPaths: string[];
}

@Injectable({
	providedIn: 'root'
})


export class ConfigService {
	public xanoApiUrl: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public authToken: BehaviorSubject<any> = new BehaviorSubject<any>(null);

	public config: XanoConfig = {
		title: 'Xano Wiki',
		summary: 'This extension provides a fully functioning demo to manage a wiki',
		editText: 'Get source code',
		editLink: '',
		descriptionHtml: `
                <h4>Description</h4>
                <p>This demo consists of following components:</p>
                <h5>Personal View</h5>
                <p>
                	This is your wiki where you see a list of posted wikis. You are able to read wikis by clicking cards in the list.
                 	You are also able to post new wikis, as well as edit or delete existing wikis once logged in.
                 </p>
                 <p>
                 	Additionally, example of version history of the wiki is included.
				</p>
                <h5>Manage View</h5>
                <p>This is a side panel that lets you add a new wiki or edit an existing wiki. The edit component also supports deleting a wiki.</p>
                <h5>Search</h5>
                <p>A search box is present to allow you to search the title, description, and content of a wiki.</p>
                `,
		logoHtml: '',
		requiredApiPaths: [
			'/wiki',
			'/wiki/{wiki_id}/history',
			'/wiki/{slug}',
			'/auth/login'
		]
	};

	constructor() {
	}

	public isConfigured(): Observable<any> {
		return this.xanoApiUrl.asObservable();
	}

	public isLoggedIn(): Observable<any> {
		return this.authToken.asObservable();
	}
}
