import {Component, EventEmitter, OnInit} from '@angular/core';
import {WikiService} from '../wiki.service';
import {faSearch, faSpinner} from '@fortawesome/free-solid-svg-icons';
import {FormGeneratorService} from '../shared/form-generator/form-generator.service';
import {debounceTime, distinctUntilChanged, finalize, tap} from 'rxjs/operators';
import {ToastService} from '../shared/toast/toast.service';
import * as _ from 'lodash-es';
import {ConfigService} from '../config.service';
import {Router} from '@angular/router';
import {LoginPanelComponent} from '../login-panel/login-panel.component';
import {PanelService} from '../shared/panel/panel.service';
import {WikiManagePanelComponent} from '../wiki-manage-panel/wiki-manage-panel.component';

export interface Wiki {
	id: number;
	created_at: number;
	title: string;
	slug: string;
	content: string;
	description: string;
	tags: string[];
}

@Component({
	selector: 'app-wiki-list',
	templateUrl: './wiki-list.component.html',
	styleUrls: ['./wiki-list.component.scss']
})
export class WikiListComponent implements OnInit {
	public wikis: Wiki[] = [];
	public form: any;
	public searchForm: any;
	public loading: boolean = true;
	public saving: boolean = false;
	public spinner: any = faSpinner;
	public search: any = faSearch;
	public loggedIn: boolean = false;

	constructor(
		private wikiService: WikiService,
		private configService: ConfigService,
		private formGeneratorService: FormGeneratorService,
		private toastService: ToastService,
		private panelService: PanelService,
		private router: Router
	) {
	}

	ngOnInit(): void {
		this.configService.isLoggedIn().subscribe(loggedIn => this.loggedIn = loggedIn);
		this.searchForm = this.createSearchForm();

		this.searchForm.valueChanges
			.pipe(
				debounceTime(300),
				distinctUntilChanged(),
			)
			.subscribe(() => {
				this.getWikiList();
			});
		this.getWikiList();
	}

	public createSearchForm(): any {
		return this.formGeneratorService.createFormGroup({
			obj: {
				search: 'text'
			},
			defaultValue: {
				search: ''
			}
		});
	}

	public getWikiList(): void {
		const search = {
			expression: []
		};

		const searchValue = this.searchForm.value.search.trim();

		if (searchValue) {
			search.expression.push({
				'type': 'group',
				'group': {
					'expression': [
						{
							'statement': {
								'left': {
									'tag': 'col',
									'operand': 'wiki.title'
								},
								'op': 'includes',
								'right': {
									'operand': `${searchValue}`
								}
							}
						},
						{
							'or': true,
							'statement': {
								'left': {
									'tag': 'col',
									'operand': 'wiki.description'
								},
								'op': 'includes',
								'right': {
									'operand': `${searchValue}`
								}
							}
						},
						{
							'or': true,
							'statement': {
								'left': {
									'tag': 'col',
									'operand': 'wiki.content'
								},
								'op': 'includes',
								'right': {
									'operand': `${searchValue}`
								}
							}
						}
					]
				}
			});
		}
		this.wikiService.wikiListGet(search).pipe(
			tap(() => this.loading = true),
			finalize(() => this.loading = false)
		).subscribe(res => {
			this.wikis = res;
		}, (err) => {
			this.toastService.error(_.get(err, 'error.message', 'An unknown error has occurred.'));
		});
	}

	public addWiki(): void {
		let onSuccess = new EventEmitter();
		onSuccess.subscribe(payload => {
			this.toastService.success('Wiki added.');
			this.getWikiList();
		});

		WikiManagePanelComponent.open(this.panelService, {
			item: null,
			successEmitter: onSuccess
		});
	}

	public logout() {
		this.router.navigate(['']).then(x => {
			this.configService.authToken.next(null);
		});
	}

	public login() {
		LoginPanelComponent.open(this.panelService, {});
	}
}
