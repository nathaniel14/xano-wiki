import {Component, EventEmitter} from '@angular/core';
import {PanelService} from '../shared/panel/panel.service';
import {PanelComponent} from '../shared/panel/panel.component';
import {ConfigService} from '../config.service';
import {FormGeneratorService} from '../shared/form-generator/form-generator.service';
import {WikiService} from '../wiki.service';
import {ToastService} from '../shared/toast/toast.service';
import {get} from 'lodash-es';
import {FormControl, Validators} from '@angular/forms';
import {finalize, tap} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
	selector: 'app-wiki-manage-panel',
	templateUrl: './wiki-manage-panel.component.html',
	styleUrls: ['./wiki-manage-panel.component.scss']
})
export class WikiManagePanelComponent extends PanelComponent {
	static ID = Symbol();
	public wikiForm: any;
	public tagInput: FormControl = new FormControl(null);
	public saving: any;
	public deleting: any;

	public modules = {
		toolbar: [
			['bold', 'italic', 'underline'],
			['blockquote'],
			[{ header: 1 }, {header: 2}],
			[{list: 'ordered'}, {list: 'bullet'}],
			[{color: []}],
			['link']
		]
	};

	constructor(
		protected panelService: PanelService,
		public configService: ConfigService,
		protected formGeneratorService: FormGeneratorService,
		protected wikiService: WikiService,
		protected toastService: ToastService,
		protected router: Router
	) {
		super(panelService);
	}

	static open(panel: PanelService, args: { item: any, successEmitter?: EventEmitter<any>, deleteEmitter?: EventEmitter<any> }) {
		panel.open(this.ID, 'Wiki', {
			...args
		});
	}

	public getTitle(): string {
		return this.data.item ? 'Edit Wiki' : 'Add Wiki';
	}

	public getDescription(): string {
		return 'Modify your wiki below';
	}

	public getLabel(): string {
		return this.data.item ? 'Update' : 'Add';
	}

	public onOpen(): void {
		this.wikiForm = this.createForm();
	}

	createForm() {
		const defaultValue = this.data.item || {};
		this.tagInput.patchValue(this.data?.item?.tags);

		return this.formGeneratorService.createFormGroup({
			obj: {
				title: ['text', [Validators.required]],
				slug: ['text', [Validators.required]],
				content: ['text', [Validators.required]],
				description: ['text', [Validators.required]],
			},
			defaultValue
		});
	}

	getComponentId() {
		return WikiManagePanelComponent.ID;
	}

	delete() {
		this.wikiService.wikiDelete(this.data.item.id)
			.pipe(
				tap(() => this.deleting = true),
				finalize(() => this.deleting = false)
			)
			.subscribe((res: any) => {
				if (this.data.deleteEmitter) {
					this.data.deleteEmitter.emit(res);
				}
				this.router.navigate(['']);
				this.close();
			}, err => {
				this.toastService.error(get(err, 'error.message', 'An error has occurred.'));
			});
	}

	save() {
		if (!this.wikiForm.trySubmit()) {
			return;
		}

		if (!this.data.item) {
			this.wikiService.wikiSave({...this.wikiForm.value, tags: this.tagInput.value})
				.pipe(
					tap(x => this.saving = true),
					finalize(() => this.saving = false)
				)
				.subscribe((res: any) => {
					if (this.data.successEmitter) {
						this.data.successEmitter.emit(res);
					}
					this.close();
				}, err => {
					this.toastService.error(get(err, 'error.message', 'An error has occurred.'));
				});
		} else {
			this.wikiService.wikiUpdate({id: this.data.item.id, ...this.wikiForm.value, tags: this.tagInput.value})
				.pipe(
					tap(() => this.saving = true),
					finalize(() => this.saving = false)
				)
				.subscribe((res: any) => {
					if (this.data.successEmitter) {
						this.data.successEmitter.emit(res);
					}
					this.close();
				}, err => {
					this.toastService.error(get(err, 'error.message', 'An error has occurred.'));
				});
		}
	}

	public patchContentInput(event) {
		this.wikiForm.controls.patchValue(event.html);
	}
}
