import {Component, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from '../config.service';
import {PanelService} from '../shared/panel/panel.service';
import {ConfigPanelComponent} from '../shared/config-panel/config-panel.component';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	constructor(
		private configService: ConfigService,
		private panelService: PanelService
	) {
	}

	public config: XanoConfig;
	private myTemplate: any = '';
	public configured: boolean = false;

	ngOnInit(): void {
		this.config = this.configService.config;
		this.configService.xanoApiUrl.subscribe(apiUrl => this.configured = !!apiUrl);
	}

	public openPanel(): void {
		ConfigPanelComponent.open(this.panelService, {});
	}
}
