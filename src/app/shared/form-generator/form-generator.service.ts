import {Injectable} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import * as _ from 'lodash-es';

@Injectable({
	providedIn: 'root'
})
export class FormGeneratorService {

	constructor(
		private formBuilder: FormBuilder,
	) {
	}

	public createFormGroup({obj = {}, defaultValue = undefined, root = true}): any {
		if (obj == 'json') {
			return this.formBuilder.control({});
		}
		if (obj === '') {
			return this.formBuilder.control('');
		}
		let formGroup: FormGroup = this.formBuilder.group({});

		obj = this.unrollSchema(obj);

		Object.keys(obj).forEach(key => {

			// this.formResult = this.formBuilder.group({
			//   result: this.formBuilder.array([])
			// });

			// let schema = obj[key];
			let schema = this.unrollSchema(obj[key]);

			let paramName = this.normalizeNode(this.getParamName(key));
			let paramDefaultValue = this.getParamDefault(key, schema);
			let paramValidators = this.getParamValidators(obj[key]);
			let isObject = this.isSchemaObject(schema);
			let isArray = this.isParamArray(key);

			if (isArray) {
				if (isObject) {
					let arrayItemDefault = schema;//paramDefaultValue;//_.get(defaultValue, key);
					let arrayList = this.formBuilder.array([]);
					Object.defineProperty(arrayList, '__DEFAULT_ITEM__', {value: _.cloneDeep(arrayItemDefault)});

					formGroup.addControl(
						paramName,
						arrayList
					);
				} else {
					let arrayItemDefault = '';//paramDefaultValue;
					// let arrayItem = this.formBuilder.control(arrayItemDefault, [Validators.required]);
					// let arrayList = this.formBuilder.array([arrayItem]);
					let arrayList = this.formBuilder.array([]);

					Object.defineProperty(arrayList, '__DEFAULT_ITEM__', {value: _.cloneDeep(arrayItemDefault)});

					formGroup.addControl(
						paramName,
						arrayList
					);
				}
			} else {
				if (isObject) {
					formGroup.addControl(
						paramName,
						this.createFormGroup({obj: schema/*, defaultValue: paramDefaultValue*/, root: false})
					);
				} else {
					formGroup.addControl(
						paramName,
						this.formBuilder.control(paramDefaultValue, paramValidators)
					);
				}
			}
		});

		if (defaultValue !== undefined) {
			this.setFormValue(formGroup, defaultValue);
			// formGroup.setValue(defaultValue);
		}

		if (root) {
			(<any> formGroup).submitted = false;
			(<any> formGroup).trySubmit = () => {
				(<any> formGroup).submitted = true;
				return formGroup.status == 'VALID';
			};

			(<any> formGroup).getErrors = name => {
				if (!(<any> formGroup).submitted) {
					return false;
				}
				let errors = formGroup.get(name).errors;
				return errors == null ? false : errors;
			};
		}

		return formGroup;
	}

	public setFormValue(formContext, defaultValue, root = true): any {
		if (!formContext) {
			return;
		}

		this.setFormValue_(formContext, defaultValue, root);

		formContext.patchValue(defaultValue);
	}

	private setFormValue_(formContext, defaultValue, root): void {
		let name = formContext.constructor.name;

		switch (name) {
			case FormGroup.name:
				for (let key in formContext.controls) {
					this.setFormValue_(formContext.controls[key], _.get(defaultValue, key), false);
				}
				break;
			case FormArray.name:
				if (defaultValue && _.isArray(defaultValue)) {
					if (formContext.controls.length != 0) {
						formContext.clear();
					}

					let schema = this.getDefaultItemFromFormArray(formContext);

					while (formContext.controls.length != defaultValue.length) {
						let group = this.createFormGroup({obj: schema, root});

						this.setFormValue_(group, defaultValue[formContext.controls.length], false);

						formContext.push(group);
					}
				}
				break;
		}
	}

	public getDefaultItemFromFormArray(formArray): any {
		return _.cloneDeep(formArray.__DEFAULT_ITEM__);
	}

	public normalizeNode(node): string {
		if (typeof node == 'string') {
			node = node.split('{}').join('').split('[]').join('');
		}
		return node;
	}

	public getParamValidators(node): any {
		if (_.isArray(node) && node.length == 2) {
			return node[1];
		}

		return [];
	}

	public isParamArray(item): boolean {
		return item.includes('[]');
	}

	public isParamNullable(param): boolean {
		return param.substring(1) == '?';
	}

	public isParamList(param): boolean {
		return param.includes('[]');
	}

	public isSchemaObject(schema): boolean {
		return typeof schema == 'object' || schema == 'json';
	}

	public getParamName(value): string {
		if (typeof value != 'string') {
			throw Error('Expecting string.');
		}

		if (value.charAt(0) == '?') {
			value = value.substring(1);
		}

		value = value.replace('[]', '');
		value = value.replace('[]', '');

		return value.replace('?', '=').split('=').shift();
	}

	public getParamDefault(value, node, defaultValue = ''): any {
		if (typeof node == 'object') {
			if (this.isParamArray(value)) {
				return [];
			}

			let obj = {};

			Object.keys(node).forEach(key => {
				let paramName = this.getParamName(key);
				if (this.isParamNullable(key)) {
					obj[paramName] = null;
				} else {
					obj[paramName] = this.getParamDefault(key, node[key]);
				}
			});

			return obj;
		}

		if (typeof value != 'string') {
			throw Error('Expecting string.');
		}

		let parts = value.split('?=');
		if (parts.length != 2) {
			if (this.isParamArray(value)) {
				return [];
			}

			if (node == 'bool') {
				return false;
			}

			return defaultValue;
		}

		let result;
		try {
			result = JSON.parse(parts[1]);
		} catch (e) {
			result = parts[1];
		}

		if (this.isParamArray(value)) {
			result = [result];
		}

		return result;
	}

	public getFormErrors(form, name): any {
		if (form['getErrors']) {
			return form.getErrors(name);
		}

		let errors;

		if (typeof name == 'number') {
			errors = form.controls[name].errors;
		} else {
			errors = form.get(name).errors;
		}

		return errors == null ? false : errors;
	}

	public unrollSchema(schema): any {
		if (_.isArray(schema) && schema.length == 2) {
			schema = schema[0];
		}

		return schema;
	}

	public resetForm(form, defaultValue = {}): void {
		form.reset();
		this.setFormValue(form, defaultValue);
		form.submitted = false;
	}

}
