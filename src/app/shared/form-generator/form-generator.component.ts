import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormGeneratorService} from './form-generator.service';
import {FormGroup} from '@angular/forms';
import * as _ from 'lodash-es';

@Component({
	selector: 'app-form-generator',
	templateUrl: './form-generator.component.html'
})
export class FormGeneratorComponent implements OnInit {

	@Input() type: string = 'text';
	@Input() class: string = 'form-control';
	@Input() placeholder: string = '';
	@Input() form: FormGroup;
	@Input() name: string;
	@Input() errors: any = {};

	_values: any[] = [];

	@Input() set values(v) {
		this._values = v;
	};

	get values() {
		return this._values;
	}

	@Input() focus = false;
	@Input() disabled: any = undefined;
	@Input() submitted;
	@Input() focus_delay = 500;
	@Output() change = new EventEmitter();

	@ViewChild('inputWrap', {static: true}) inputWrapField: ElementRef;

	static ID = Symbol();

	constructor(
		private formGeneratorService: FormGeneratorService
	) {
	}

	ngOnInit(): void {
		if (this.focus) {
			if (['text', 'textarea'].includes(this.type)) {
				setTimeout(() => {
					if (this.inputWrapField && this.inputWrapField.nativeElement && this.inputWrapField.nativeElement.firstElementChild) {
						this.inputWrapField.nativeElement.firstElementChild.focus();
					}
				}, this.focus_delay);
			}
		}
	}

	public getDisabled(): boolean {
		return this.disabled ? true : undefined;
	}

	public hasErrors(): any {
		if (typeof this.submitted == 'boolean') {
			if (!this.submitted) {
				return false;
			}
		}

		return this.getFormErrors();
	}

	public onChange(event): void {
		if (typeof this.name == 'number') {
			this.change.emit(this.form.controls[this.name].value);
		} else {
			this.change.emit(this.form.get(this.name).value);
		}
	}

	public getFormErrors(): any {
		return this.formGeneratorService.getFormErrors(this.form, this.name);
	}

	public getValue(value): any {
		return _.get(value, 'value', value);
	}

	public getDisplay(value): any {
		return _.get(value, 'display', this.getValue(value));
	}

	public error(type): any {
		let ret = this.getFormErrors();
		if (ret[type]) {
			if (Object.keys(ret)[0] == type) {
				return ret[type];
			}
		}

		return false;
	}
}
