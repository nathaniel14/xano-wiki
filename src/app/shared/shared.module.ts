import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PanelFooterComponent} from './panel/panel-footer/panel-footer.component';
import {PanelHeaderComponent} from './panel/panel-header/panel-header.component';
import {PanelInfoComponent} from './panel/panel-info/panel-info.component';
import {FormGeneratorComponent} from './form-generator/form-generator.component';
import {ToastComponent} from './toast/toast.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {SafeHtmlPipe} from './safe-html.pipe';
import {ConfigPanelComponent} from './config-panel/config-panel.component';
import {NgbToastModule} from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
	declarations: [
		PanelFooterComponent,
		PanelHeaderComponent,
		PanelInfoComponent,
		FormGeneratorComponent,
		ToastComponent,
		SafeHtmlPipe,
		ConfigPanelComponent
	],
	exports: [
		ConfigPanelComponent,
		ToastComponent,
		SafeHtmlPipe,
		PanelHeaderComponent,
		PanelInfoComponent,
		FormGeneratorComponent,
		PanelFooterComponent
	],
	imports: [
		CommonModule,
		FontAwesomeModule,
		NgbToastModule,
		ReactiveFormsModule
	]
})
export class SharedModule {
}
