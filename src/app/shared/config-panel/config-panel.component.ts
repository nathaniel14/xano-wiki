import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ToastService} from '../toast/toast.service';
import {PanelComponent} from '../panel/panel.component';
import {PanelService} from '../panel/panel.service';
import {ConfigService} from '../../config.service';
import {FormGeneratorService} from '../form-generator/form-generator.service';
import {ApiService} from '../../api.service';
import {XanoService} from '../../xano.service';

@Component({
	selector: 'app-config-panel',
	templateUrl: './config-panel.component.html'
})
export class ConfigPanelComponent extends PanelComponent {

	static ID: Symbol = Symbol();
	public setupForm: any;
	public saving: any;

	constructor(
		protected panelService: PanelService,
		public configService: ConfigService,
		protected formGeneratorService: FormGeneratorService,
		protected apiService: ApiService,
		protected toastService: ToastService,
		protected xanoService: XanoService
	) {
		super(panelService);
	}

	onOpen() {
		this.setupForm = this.createForm();
	}

	public createForm(): FormGroup | FormControl {
		return this.formGeneratorService.createFormGroup({
			obj: {
				xanoApiUrl: ['text', [Validators.required, Validators.pattern('^https?://.+')]],
			},
			defaultValue: {
				xanoApiUrl: this.configService.xanoApiUrl.value
			}
		});
	}

	public getComponentId(): Symbol {
		return ConfigPanelComponent.ID;
	}

	static open(panel: PanelService, args: {}) {
		panel.open(this.ID, 'Setup', {...args});
	}

	public save(): any {
		if (!this.setupForm.trySubmit()) {
			return;
		}

		this.apiService.get({
			endpoint: this.xanoService.getApiSpecUrl(this.setupForm.value.xanoApiUrl),
			headers: {
				Accept: 'text/yaml'
			},
			responseType: 'text',
			stateFunc: state => this.saving = state
		}).subscribe((res: any) => {
			if (!this.configService.config.requiredApiPaths.every(path => res.includes(path))) {
				this.toastService.error('This Xano Base URL is missing the required endpoints. Have you installed this marketplace extension?');
				return;
			}
			this.configService.xanoApiUrl.next(this.xanoService.getApiUrl(this.setupForm.value.xanoApiUrl));
			this.close();
		}, err => {
			this.toastService.error('An error has occurred.');
		});
	}
}
