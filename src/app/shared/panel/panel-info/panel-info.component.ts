import {Component, Input, OnInit} from '@angular/core';

@Component({
	selector: 'app-panel-info',
	templateUrl: './panel-info.component.html',
	styleUrls: ['./panel-info.component.scss']
})
export class PanelInfoComponent implements OnInit {

	@Input() title: string;
	@Input() description: string;
	@Input() classes: any;
	@Input() icon: any;

	static ID: Symbol = Symbol();

	constructor() {
	}

	ngOnInit(): void {
	}

}
