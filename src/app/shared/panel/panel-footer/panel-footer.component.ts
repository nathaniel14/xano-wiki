import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {faSave, faSpinner, faTrashAlt} from '@fortawesome/free-solid-svg-icons';

@Component({
	selector: 'app-panel-footer',
	templateUrl: './panel-footer.component.html',
	styleUrls: ['./panel-footer.component.scss']
})
export class PanelFooterComponent implements OnInit {
	@Output() onSave: EventEmitter<any> = new EventEmitter;
	@Output() onDelete: EventEmitter<any> = new EventEmitter;
	@Output() onGoBack: EventEmitter<any> = new EventEmitter;

	@Input() disabled: boolean = false;
	@Input() deleting: boolean = false;
	@Input() saving: any;
	@Input() saveType: string = 'button';
	@Input() saveIcon = faSave;
	@Input() saveLabel: string = 'Save';
	@Input() saveClass: string = 'btn ml-auto btn-primary';
	@Input() trashIcon = faTrashAlt;
	@Input() trashLabel: string = '';

	public spinner: any = faSpinner;

	static ID: Symbol = Symbol();

	constructor() {
	}

	ngOnInit(): void {
	}

	public isSaving(): any {
		let ret = this.saving;
		while (typeof ret == 'function') {
			ret = ret();
		}
		return ret;
	}

	public hasProperty(emitter: EventEmitter<any>): boolean {
		return emitter?.observers.length > 0;
	}

	public emitEvent(emitter: EventEmitter<any>): void {
		return emitter?.emit();
	}
}
