import {EventEmitter, Injectable} from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class PanelService {
	public data: any = {};
	public component: any = {};
	public itemUpdated$: EventEmitter<any> = new EventEmitter();

	constructor() {
	}


	public observe(): EventEmitter<any> {
		return this.itemUpdated$;
	}

	public open(key, component, data): void {
		this.component[key] = component;

		this.itemUpdated$.emit({key, open: true, data});
	}

	close(key): void {
		this.itemUpdated$.emit({key, open: false});
	}
}
