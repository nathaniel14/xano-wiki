import {Component, OnInit} from '@angular/core';
import {ConfigService} from '../../config.service';
import {PanelService} from '../panel/panel.service';
import {ConfigPanelComponent} from '../config-panel/config-panel.component';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {
	public apiConfigured: boolean = false;

	constructor(
		private configService: ConfigService,
		private panelService: PanelService
	) {
	}

	ngOnInit(): void {
		this.configService.isConfigured().subscribe(apiUrl => {
			this.apiConfigured = !!apiUrl;
		});
	}


	public openConfigPanel() {
		ConfigPanelComponent.open(this.panelService, {});
	}
}
