import {Injectable, TemplateRef} from '@angular/core';

@Injectable({
	providedIn: 'root'
})

export class ToastService {
	toasts: any[] = [];

	static ID: Symbol = Symbol();

	constructor() {
	}

	public show(textOrTpl: string | TemplateRef<any>, options: any = {}): void {
		this.toasts.push({textOrTpl, ...options});
	}

	public success(text: string): void {
		this.show(text, {
			classname: 'bg-success text-light',
			headertext: text,
			delay: 6000,
			autohide: true
		});
	}

	public error(text: string): void {
		this.show(text, {
			classname: 'bg-danger text-light',
			headertext: text,
			delay: 6000,
			autohide: true
		});
	}

	public remove(toast): void {
		this.toasts = this.toasts.filter(t => t !== toast);
	}
}
