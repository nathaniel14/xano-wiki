import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './shared/header/header.component';
import {HomeComponent} from './home/home.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {SharedModule} from './shared/shared.module';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {HttpClientModule} from '@angular/common/http';
import {WikiListComponent} from './wiki-list/wiki-list.component';
import {WikiViewComponent} from './wiki-view/wiki-view.component';
import {WikiManagePanelComponent} from './wiki-manage-panel/wiki-manage-panel.component';
import {ReactiveFormsModule} from '@angular/forms';
import {LoginPanelComponent} from './login-panel/login-panel.component';
import {TagInputModule} from 'ngx-chips';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {QuillModule} from 'ngx-quill';

@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		HeaderComponent,
		WikiListComponent,
		WikiViewComponent,
		WikiManagePanelComponent,
		LoginPanelComponent
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		AppRoutingModule,
		SharedModule,
		HttpClientModule,
		QuillModule.forRoot(),
		TagInputModule,
		NgbModule,
		FontAwesomeModule,
		ReactiveFormsModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
}
